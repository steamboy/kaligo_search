# README

## Introduction
Kaligo Technial Exam

## Setup

### Go to project folder
- cd kaligo_search

### Install bundler gem
- gem install bundler

### Run bundler
- bundle install

### Install Redis server
- brew install redis (mac)

## Running the app

### Start web server
- rails server

### Start Redis Server
- redis-server

### Running the test suite
- rspec

### Enabling/disabling the cache (just run the command to toggle caching)
- rails dev:cache

## Endpoints with sample parameters
- http://localhost:3000/search
- http://localhost:3000/search?suppliers=2tlb8
- http://localhost:3000/search?suppliers=2tlb8,42lok
- http://localhost:3000/search?destination=Singapore
- http://localhost:3000/search?guests=5
- http://localhost:3000/search?checkin=2018-10-05
- http://localhost:3000/search?checkout=2018-10-06
- http://localhost:3000/search?suppliers=42lok&checkin=2018-10-05&checkout=2018-10-06&destination=Singapore&guests=10