# frozen_string_literal: true

class SearchController < ApplicationController
  def index
    search = SearchService.new(request.GET)
    cache_key = search.set_cache_key
    Rails.logger.info "cache_key: #{cache_key}"
    # Cache expires in 5mins
    json = Rails.cache.fetch(cache_key, expires_in: 5.minutes) do
      search.process
    end
    render json: json.empty? ? { message: 'Supplier must be invalid.' } : json
  end
end
