# frozen_string_literal: true

class SearchService
  require "httparty"

  def initialize(params)
    @params = params
    @suppliers = set_suppliers(@params["suppliers"])
  end

  def process
    # Fetch suppliers from api
    responses = []
    @suppliers.each do |supplier|
      if valid_supplier?(supplier)
        response = fetch_supplier(supplier)
        responses << response.map{ |key, value| { id: key, price: value, supplier: supplier } }
      end
    end

    # Group by ids
    grouped_responses = responses.flatten.group_by { |x| x[:id] }.values

    # Sort by price and get lowest price
    results = grouped_responses.map{ |grouped_response| grouped_response.min_by { |x| x[:price] } }
    return results
  end

  def set_cache_key
    # Generate cache key base from search params
    cache_key = @params.map{ |key, value| "#{key}-#{value}" if APP_CONFIG["search_params"].include?(key) }.compact
    cache_key.concat(@suppliers)
    cache_key = cache_key.sort.join("-").downcase
    return cache_key
  end

  private

    def set_suppliers(suppliers)
      return (suppliers) ? (suppliers.include?(",") ? suppliers.split(",") : suppliers.split) : APP_CONFIG["suppliers"]
    end

    def fetch_supplier(supplier)
      base_api_url = "https://api.myjson.com/bins/"
      response = HTTParty.get("#{base_api_url}/#{supplier}")
      return response.parsed_response
    end

    def valid_supplier?(supplier)
      APP_CONFIG["suppliers"].include?(supplier)
    end
end
