# frozen_string_literal: true

require "rails_helper"

RSpec.describe SearchController, type: :request, focus: true do
  describe "External request" do
    it "queries json response from MyJson" do
      uri = URI("https://api.myjson.com/bins/2tlb8")
      response = Net::HTTP.get(uri)
      expect(response).to be_an_instance_of(String)
    end
  end

  describe "GET /search" do
    it "returns 200 OK status" do
      get "/search"
      expect(response).to have_http_status(:ok)
    end

    it "returns response with search parameters" do
      get "/search?suppliers=2tlb8"
      json = JSON.parse(response.body)
      expect(json.first["id"]).to eq("abcd")
      expect(json.first["price"]).to eq(300.2)
      expect(json.first["supplier"]).to eq("2tlb8")
    end
  end
end
